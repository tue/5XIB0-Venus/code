#ifndef COMM_H
#define COMM_H

typedef enum {
	RESET1      = 0x81,
	RESET2      = 0x82,
	ACK1        = 0x81,
	ACK2        = 0x82,
	OUTOFSYNC   = 0x83,
	PING        = 0x80,
	PONG        = 0x80,
	DATA_ESCAPE = 0x84,
} serial_control_command_t;

// prefix for Debug Data packets
#define DATA_DEBUG_PREFIX 0xC0
// maximum size of a data packet
#define DATA_MAX_SIZE     256

typedef enum {
	// waiting for RESET1
	SERIAL_UNKNOWN,
	// received RESET1, sent ACK1, waiting for RESET2
	SERIAL_INIT,
	// received RESET2, sent ACK1, waiting for other commands
	SERIAL_READY,
} serial_state_t;
extern serial_state_t serial_state;

void handle_serial(data_t *data, int changedBits);
void serial_print_debug(const char *str);

/* simple wrappers for Serial.print compatibility */
static inline void serial_print_debug(String str) {
	serial_print_debug(str.c_str());
}
static inline void serial_print_debug(int n) {
	serial_print_debug(String(n));
}
static inline void serial_print_debug(long n) {
	serial_print_debug(String(n));
}
static inline void serial_print_debug(float n) {
	serial_print_debug(String(n));
}

#endif /* !COMM_H */

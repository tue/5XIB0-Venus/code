#ifndef DATATYPES_H
#define DATATYPES_H

// ****************
// ** Data types **
// ****************

// Left and Right
typedef enum {
	L,
	R
} lr_t;

// Sensor data
typedef struct {
	bool IR_left_detected;
	bool IR_right_detected;
	bool sample_gripper_detected;
	bool sample_turret_detected[NUM_TURRET_DIRECTIONS];
	bool beacon_detected[NUM_TURRET_DIRECTIONS];
	long obstacle_turret_distances[NUM_TURRET_DIRECTIONS];
	int compass;
	int robot_x[NUM_TURRET_DIRECTIONS];
	int robot_y[NUM_TURRET_DIRECTIONS];
	int robot_deg[NUM_TURRET_DIRECTIONS];
	float robot_curr_x;
	float robot_curr_y;
	int robot_curr_deg;
} data_t;
enum {
	CHANGED_SENS_COMPASS = 1 << 0,
	CHANGED_SENS_OBSTACLETURRET = 1 << 1,
	CHANGED_SENS_IRLEFT = 1 << 2,
	CHANGED_SENS_IRRIGHT = 1 << 3,
	CHANGED_SENS_SAMPLETURRET = 1 << 4,
	CHANGED_SENS_SAMPLEGRIPPER = 1 << 5
};

// Raspberry Pi Data type declarations
typedef enum {
	PI_DATATYPE_COMPASS, 
	PI_DATATYPE_OBSTACLETURRET, 
	PI_DATATYPE_IRLEFT, 
	PI_DATATYPE_IRRIGHT, 
	PI_DATATYPE_SAMPLETURRET, 
	PI_DATATYPE_SAMPLEGRIPPER, 
	PI_DATATYPE_BEACONTURRET, 
	PI_DATATYPE_LAB, 
	PI_DATATYPE_MOVETO, 
	PI_DATATYPE_GRIPPER, 
	PI_DATATYPE_TURRET,
	PI_DATATYPE_ALL
} pi_datatype_t;

// Operation modes
typedef enum {
	OPMODE_WAIT,
	OPMODE_INITIALSEQUENCE,
	OPMODE_RANDOM,
	OPMODE_MAPPING,
	OPMODE_CHECKSAMPLE,
	OPMODE_GRABSAMPLE,
	OPMODE_GOTOLABLOCATION,
	OPMODE_WAITFORLAB,
	OPMODE_FINDMAGNET,
	OPMODE_LABSEQUENCE,
	OPMODE_ERROR
} opmode_t;

#endif /* !DATATYPES_H */

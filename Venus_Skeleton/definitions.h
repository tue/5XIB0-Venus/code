// *****************
// ** DEFINITIONS **
// *****************

// Pin configuration
#define PIN_SENS_OBSTACLE_TURRET 9
#define PIN_SENS_IR_LEFT A1
#define PIN_SENS_IR_RIGHT A0
#define PIN_SENS_SAMPLE_TURRET 5
#define PIN_SENS_SAMPLE_GRIPPER 6
#define PIN_SENS_BEACON_TURRET 3
#define PIN_SERVO_LEFT 12
#define PIN_SERVO_RIGHT 13
#define PIN_SERVO_TURRET 11
#define PIN_SERVO_GRIPPER 10
// Compass is on A4, A5

// Number of directions the robot can choose from: ((x-1) * (360/x)) degree. Preferably a multiple of 4, I think.
#define NUM_DIRECTIONS 12

// Number of directions the turret can measure from
#define NUM_TURRET_DIRECTIONS 13

// Number of compass checks to create average
#define NUM_COMPASS_CHECKS 12

// Threshold value for obstacle turret. No free path!
#define OBSTACLE_TOO_CLOSE 30
// A very large ("infinity") distance value to an obstacle. It basically means
// that no obstacle is detected.
#define OBSTACLE_FAR_AWAY 300

// Maximum time between communications to Raspberry Pi (ms)
#define MAX_COMMUNICATION_DELAY 3000

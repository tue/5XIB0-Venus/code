#ifndef CALIBRATION_WALL_E_H
#define CALIBRATION_WALL_E_H
// WALLY configuration file

#define calObstacleTurretMaxDist 50
#define calObstacleTurretMinDist 10
#define CAL_IR_LEFT_THRESHOLD 700
#define CAL_IR_RIGHT_THRESHOLD 600
#define CAL_SAMPLE_GRIPPER_THRESHOLD 1
#define CAL_SAMPLE_TURRET_THRESHOLD 1

// Distance to drive during initial sequence. (to (x, y) (0, 0))
#define CAL_INITIAL_DISTANCE1 30.0
// Distance to drive during initial sequence (to starting position - one robot further then the other, to prevent collissions)
#define CAL_INITIAL_DISTANCE2 1.0

#define CAL_TURRET_DEGREE 10

// Left wheel calibration factor   WALL-E: 1.4 EVE: 1.0
#define CAL_SERVO_LEFT 1.4
// Right wheel calibration factor  WALL-E: 1.0 EVE: 1.1
#define CAL_SERVO_RIGHT 1.0

// Movement calibration
// Turn calibration (ms per degree)
#define CAL_MOVE_TURN 6.05
// Straight calibration ((1/ms) per cm)
#define CAL_MOVE_STRAIGHT 0.0154

#define CAL_TURRET_TO_CENTER_DIST 6

#define CAL_SAMPLE_TURRET_MAX_DIST 27

#endif /* !CALIBRATION_WALL_E_H */

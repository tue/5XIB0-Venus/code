#!/usr/bin/env python2
import logging
import glob
import os.path
import serial
import termios
import time
from threading import Thread

from . import comm_arduino

def main(path):
    # TODO consider using a thread for serial I/O and route finding
    comm_arduino.main()

if __name__ == '__main__':
    logging.basicConfig(format='%(asctime)s: %(message)s', level=logging.DEBUG)
    main();
